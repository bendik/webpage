title: Develop
----
description: Can I hear a BINGO!?
----
background: rgb(208, 95, 113)
----
color: white
----
public: true
----
position: 2 
----
links:
  - GitHub https://github.com/bendik
  - GitLab https://gitlab.com/bendik
  - LinkedIn https://linkedin.com/in/benlyn
----
technologies:
  - HTML
  - CSS
  - SVG
  - Javascript
  - DOM
  - Manifest.json
  - Service Workers
  - IndexDB
  - Node
  - Rust
  - Ruby
  - React
  - GraphQL
  - MongoDB
  - Postgres 
  - UNIX
  - Systemd
  - Hyperspace
  - Mapbox
  - AWS
  - GCP
