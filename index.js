const choo = require("choo")
const html = require("choo/html")
const nanocontent = require("nanocontent")
const css = require("sheetify")

const app = choo()
const content = nanocontent.readSiteSync("./content", { parent: "content" })

css("tachyons")
css`
  @media (prefers-color-scheme: dark) {
    .white {
      color: black;
    }
    .black {
      color: white;
    }
    .bg-white {
      background-color: black;
    }
    .bg-black {
      background-color: white;
    }
  }
  :root {
    --headline-font: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica, Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
    --content-font: "Public Sans", sans-serif;
    --font-size: calc(2.2vmin + 4pt);
    font-size: var(--font-size);
    font-family: var(--content-font);
    overflow-x: hidden;
    letter-spacing: .05em;
  }
  h1, h2, h3, h4, h5 {
    font-family: var(--headline-font);
    letter-spacing: .10em;
    font-weight: 900;
  }
  .bm-multiply { mix-blend-mode: multiply; }
  .writing-vrl { writing-mode: vertical-rl; }
  @media (orientation: landscape) {
    .aagaard {
      display: none;
    }
  }
  .unselectable { user-select: none; }
  .invisible { visibility: hidden; }
  .sticky { position: sticky; }
  .self-start { align-self: flex-start; }
  .draw {
    --path-length: 13551;
    --path-count: 2;
    stroke-dasharray: calc(0.1 * var(--path-length) / var(--path-count)) calc(0.4 * var(--path-length) / var(--path-count));
    stroke-width: 10pt;
    stroke: currentColor;
    transition: stroke-dasharray 1s ease, stroke-dashoffset 1s ease, opacity 1s ease;
    fill: none;
  }
  .draw.illustrate { stroke-dasharray: 0 622 954 2222 1101 8065; }
  .draw.augment { stroke-dasharray: 0 9610 567 2122 504 80000; }
  .draw.visualize { stroke-dasharray: 0 6001 895 1006 867 8000; }
  .draw.develop { stroke-dasharray: 0 2379 526 7927 526 8065; }
  nav a.link { border-left-width: .5em; }
  .mbm-color-dodge { mix-blend-mode: color-dodge; }
  .mbm-multiply { mix-blend-mode: multiply; }
  .mbm-screen { mix-blend-mode: screen; }
  .mbm-darken { mix-blend-mode: darken; }
  .spin { animation-name: spin; animation-timing-function: linear; animation-iteration-count: infinite; }
  .fade-in { animation-name: fadein; animation-timing-function: ease; }
  .a-fill-both { animation-fill-mode: both; }
  .fit-cover { object-fit: cover; }
  .fit-contain { object-fit: contain; }
  *:hover + .sibling-hover-fadein { animation-name: fadein; animation-timing-function: ease-out; }
  a:focus, a:active, a:link, a:visited { outline: 0 !important; }
  @keyframes spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }
  @keyframes fadein {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
  .t-slow {
    transition-timing-duration: 300ms;
  }
  .a-duration-normal {
    animation-duration: 300ms;
  }
  .a-duration-slowest {
    animation-duration: 10s;
  }
  .a-fill-both {
    animation-fill-mode: both;
  }
  .fit-cover {
    object-fit: cover;
  }
  .fit-contain {
    object-fit: contain;
  }
  .max-h-75vh { max-height: 75vh; }
  *:hover + .sibling-hover-fadein {
    animation-name: fadein;
    animation-timing-function: ease-out;
  }
  a:focus, a:active, a:link, a:visited {
    outline: 0 !important;
  }
  @keyframes spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }
  @keyframes fadein {
    from {
      opacity: 0;
    }
    to {
      opacity: 1;
    }
  }
`

if (process.env.NODE_ENV !== "production") app.use(require("choo-devtools")())
for (const path in content) {
  if (path === "/") continue
  app.route(path, mainView)
}
app.use(init(content))
app.route("/", mainView)
app.route("/404", fourOhFour)
if (!module.parent) app.mount("body")
else module.exports = app

function init(content) {
  return function (state, emitter, app) {
    Object.assign(state.events, {
      ANNOTATE: "annotate",
    })
    state.pages = content
    state.annotated = "bendik aagaard lynghaug"
    state.title = "What do you like to do?"
    app.route("/*", fourOhFour)
    emitter.on(state.events.ANNOTATE, (text) => {
      state.annotated = text
      emitter.emit(state.events.RENDER)
    })
    emitter.on(state.events.DOMCONTENTLOADED, () => {
      state.returning = window.localStorage.getItem("returning") ||
        window.localStorage.setItem("returning", "true")
      emitter.emit(state.events.RENDER)
    })
    emitter.on(state.events.PUSHSTATE, () => {
      state.returning = "true"
    })
  }
}

function mainView(state, emit) {
  const index = state.pages["/"] || {}
  const isRoot = !state.href
  const page = state.pages[state.href] || {}
  return html`
    <body class="black flex flex-nowrap items-start bg-white animate-bg relative mw-100 h-100">
      <h1 rel="full name" 
        onclick=${() => isRoot || emit(state.events.PUSHSTATE, "/")} 
        onmouseover=${() =>
    !isRoot || emit(state.events.ANNOTATE, index.person.toLowerCase())} 
        class="sticky top-0 bottom-0 unselectable h-100 writing-vrl pv4 ph3 tc mv0 z-1 flex-none flex justify-between ${
    state.href && "pointer"
  }"
      >
        ${
    index.person.split(" ").map((name) =>
      html`<span class=${name.toLowerCase()}>${name}</span>`
    )
  }
      </h1>
      <div id="annotations"
        class="vh-100 sticky o-1 ${
    isRoot || "o-0"
  } top-0 flex justify-center items-center pv4 ph0"
        style="
          flex-basis: ${isRoot ? "calc(100vw - 8em)" : 0};
          transition: opacity 1s ease, flex-basis 300ms ease;
        ">
        <svg
          class="overflow-hidden ${
    state.annotated === "bendik aagaard lynghaug"
      ? "aspect-ratio w-0"
      : "w-75 max-h-75vh"
  }"
          viewBox="-10 -20 474 526"
          preserveAspectRatio="xMidYMid meet"
        >
          <path 
            class="draw ${state.annotated}" 
            d="M192.4,496.6c66.8-.8,276.5-472.5,163.9-449.1s-10.2,27.6-10.2,27.6c15.5,6.4,30.3,16.9,44.5,32.7L245,431.4l-64.8,56.7L172.1,391,309.7,67.3h-.5S48.7-27,244.7,166.5,85.2,210.2,210,337s33.5,161.2,104.3,160.8c46.9,0,64.9-4.7,70.6-55,3.1-27.6,1.3-104,1.3-119,0-42,37.4-45.3,37.4-45.3s-37.4-3.2-37.4-44.8c0-14.7,1.8-90.3-1.3-117.6-5.7-49.7-23.7-54.4-70.6-54.4-166.4-.6-465.7,76.6-117.1,170.2S14.7,361.1,173.4,388.1l30.5,6.9,138-322c-21.1-8.2-32.6-3.8-32.7-5.7,5.3-11.6,3-32.5,56.4-9.4,43.7,19,33.4,36.7,25,49.9-3.7-6.4-11-14-20.5-19.5L229.2,409.1,203.9,395l25.3,14.1L245,431.4l-49.1,43-17.2-4.5-.4,26.7c170.6,3.2,416.7-36.2,41.1-262.2s259-62,204.8,0l-39.3,44.5L350.2,147.1l-38.6-41.3-35.3,40.5L241,278.9l-35.3,82.6L135,408.4,98.4,242.5l-46,79.3c-107,183.8,295.8,210.7,185.9-77.9S533.7,498.1,457.5,497.4H24.4V61.7s1.5-51.8,81.1-49.8S375.2,76.7,224.8,201.4,415.1,438,457.5,315.4c12-26.7,0-48.6,0-48.6l-73.9-41.3H95.7l-74,41.3s-18,21.9,0,48.6c0,0,86.5,172.6,200.4-16.9S364,59.8,169,60.5c-46.9,0-64.9,4.7-70.6,55-3.2,27.6-1.4,104-1.4,118.9,0,42.1-37.3,45.4-37.3,45.4S97,283,97,324.6c0,14.7-1.8,90.3,1.4,117.6,5.7,49.7,23.7,54.4,70.6,54.4h-2.3c182.9.8,90.5-459.3-34.3-235.6S383.9,3.5,382.3,225.5v51s3.4,14.6-67.9,14.6c-57.3,0-58.7-17-58.7-17V225.5H227l-.7,52.6s-1.3,13-68.6,13c-62.1,0-62-20.7-62-20.7V225.5c0-129.3,336.8,285.3,208.7,182.9s-131.8,89.8-18.7,89.4Z"
            transform="translate(-14.2 -14.3)"
            fill="none"
          />
        </svg>
        <section class="tc flex-column ${
    state.annotated === "bendik aagaard lynghaug" && isRoot ? "flex" : "dn"
  }">
          <h2 class="dib">
            Hello <br/>
            <span 
              class=${state.returning || "dn"}
            >
              again
            </span>
          </h2>
          <p class="o-0 sibling-hover-fadein a-fill-both a-duration-normal mw7">${index.speech}</p>
        </section>
      </div>
      <nav rel="abilities" 
        class="h-100 sticky top-0 writing-vrl b flex pv4 justify-between"
        style="transform: translateX(.5px) rotate(180deg);"
      >
        ${
    Object.values(state.pages).filter((p) => p.url !== "/").map((p) =>
      html`
          <a
            href=${p.url} 
            onmouseenter=${() =>
        !isRoot || emit(state.events.ANNOTATE, p.url.slice(1))}
            class="link black di outline-0 ph3 pv1 bl order-${p.position}"
            style="border-color: ${p.background}"
          >${p.title}</a>
        `
    )
  }
      </nav>
      ${renderContent(page)}
    </body>
  `
}
function renderContent(page) {
  console.log(page)
  const isIndexPage = !page.url
  return html`
    <main
      class="self-stretch flex items-center overflow-x-hidden"
      style="
        transition: background 1s ease;
        flex-basis: ${isIndexPage ? "0" : "calc(100vw - 8em)"}; 
        background-color: ${page.background || "transparent"}
      "
    >
      <div class="${
    isIndexPage ? "dn" : "flex"
  } ph3 flex-column items-center pv2" style="width: calc(100vw - 8em);">
        <p class="b tc">${page.description}</p>
        ${
    page.technologies
      ? html`
          <ul class="list ph4 flex flex-wrap justify-center">
            ${
        page.technologies.map((tech) =>
          html`<li class="bg-white br2 pa2 ma1 o-70">${tech}</li>`
        )
      }
          </ul>
        `
      : null
  }
        ${page.links ? page.links.map(renderLink) : null}
        <section rel="files" class="w-100 pa2 flex justify-center flex-wrap">
          ${page.title ? Object.values(page.files).map(renderFile) : null}
        </section>
      </div>
    </main>
  `
}
function renderLink(string) {
  const [name, href] = string.split(" ")
  return html`<a target="_blank" rel=${name} class="bg-green link black mr2 pa2 ma1 br2" href=${href}>${name}</a>`
}
function renderFile(file, _, files) {
  if (file.filename.indexOf("resume") > -1) {
    return html`<a target="_blank" rel="noopener noreferrer" href=${file.source} class="db pa2 link black br2 bg-yellow">📄 Resume</a>`
  }
  if (file.extension === ".html") {
    return html`<iframe class="bn br2 h5 ma2 mw5-l w-100" src=${file.source} loading="lazy" />`
  }
  if (file.extension === ".jpg") {
    return html`<picture class="flex bn br2 ma2 h5 mw5-l w-100 mbm-multiply overflow-hidden justify-center"><img class="fit-contain" alt=${file.filename} src=${file.source} loading="lazy" /></picture>`
  }
  if (file.extension === ".avif") {
    return html`<picture class="flex bn br2 ma2 h5 mw5-l w-100 mbm-multiply overflow-hidden justify-center"><source srcset="${file.source}" type="image/avif" loading="lazy" /><img class="fit-contain" alt=${file.filename} src=${
      file.source.slice(0, -4) + "jpg"
    } loading="lazy" /></picture>`
  }
  if (file.extension === ".png") {
    return html`<div class="bn br2 ma2 h5 mw5-l w-100 mbm-multiply overflow-hidden"><img class="fit-contain" alt=${file.filename} src=${file.source} loading="lazy" /></div>`
  }
  if (file.extension === ".svg") {
    return html`<div class="flex bn br2 ma2 h4 mw4-l w-100 overflow-hidden justify-center"><img class="fit-contain" alt=${file.filename} src=${file.source} loading="lazy" /></div>`
  }
}

function fourOhFour() {
  return html`<div>Four! Oh, four.</div>`
}
